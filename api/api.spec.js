/*jshint esnext: true */

import PeachAPI from './api';

describe('Provider: Peach API', () => {
  let provider, sandbox, $httpBackend, $http, $q, $cookies, $peachSession,
      MockCookies, MockPeachSession, MockCookieObject, isDevelopment, isPreview, activeApp;

  let apiURL = 'https://my.api.url.com/v1';

  beforeEach(angular.mock.module('ng', ($provide) => {

    MockCookies = {
      get: () => { return true; },
      getObject: () => { return MockCookieObject; },
      put: () => { return true; },
      putObject: () => { return true; },
      remove: () => { return true; }
    };

    MockPeachSession = {
      getActiveAccount: () => {
        return 99;
      },
      getActiveApp: () => {
        return activeApp;
      },
      getApiUrl: () => {
        return apiURL;
      },
      getDevSessionCookieName: () => {
        return 'dev_cookie';
      },
      getSessionAppTokensCookieName: () => {
        return 'peach_app_tokens';
      },
      getSessionCookieName: () => {
        return 'cookie';
      },
      isDevelopment: () => {
        return isDevelopment;
      },
      isPreview: () => {
        return isPreview;
      }
    };

    $provide.value('$peachSession', MockPeachSession);
    $provide.value('$cookies', MockCookies);
  }));

  beforeEach(inject((_$httpBackend_, _$http_, _$q_, _$cookies_, _$peachSession_) => {
    $httpBackend = _$httpBackend_;
    $http = _$http_;
    $q = _$q_;
    $cookies = _$cookies_;
    $peachSession = _$peachSession_;
    provider = new PeachAPI.provider();

    sandbox = sinon.sandbox.create();

    activeApp = 'myApp';
    isDevelopment = false;
    MockCookieObject = {};
  }));

  afterEach(() => {
    sandbox.restore();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should construct a Peach API Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachAPI.provider(); }).to.throw('Cannot call a class as a function');
  });

  describe('Service: Peach API', () => {
    let $peachAPI;

    beforeEach(() => {
      $peachAPI = provider.$get($http, $q, $cookies, $peachSession);
    });

    it('should construct a $peachAPI service', () => {
      expect($peachAPI).not.to.equal('undefined');
    });

    it('should get a new Object API', () => {
      let Dog = $peachAPI('dogs');
      expect(Dog).to.have.property('create');
      expect(Dog).to.have.property('find');
      expect(Dog).to.have.property('findAndRemove');
      expect(Dog).to.have.property('findAndUpdate');
      expect(Dog).to.have.property('remove');
      expect(Dog).to.have.property('save');
      expect(Dog).to.have.property('update');
    });

    it('should get a new Object API from a defined path', () => {
      let Dog = $peachAPI('/dogs');
      expect(Dog).to.be.an('object');
    });

    it('should get a new Object API while setting options', () => {
      let Dog = $peachAPI('dogs', {'id': 1234});
      expect(Dog).to.be.an('object');
    });

    it('should throw an error when getting a new Object API with bad GET parameters', () => {
      expect(() => { $peachAPI('dogs', 1234); }).to.throw('API options must be an object.');
    });

    it('should get a new Object API in development mode', () => {
      sandbox.stub($peachSession, 'isDevelopment').returns(true);
      let stub = sandbox.stub($cookies, 'get').returns('1234');

      let Dog = $peachAPI('dogs');
      sinon.assert.calledOnce(stub);
    });

    it('should get a new Object API while using the app key', () => {
      activeApp = 'myApp';
      MockCookieObject = {'myApp': {'access_token': 'abc123'}};

      let stub = sandbox.stub($peachSession, 'getSessionCookieName').returns(true);

      let Dog = $peachAPI('dogs');
      sinon.assert.notCalled(stub);
    });

    it('should get a new Object API while using the core key', () => {
      activeApp = 'myApp';
      MockCookieObject = {'myOtherApp': 'abc123'};

      let stub = sandbox.stub($peachSession, 'getSessionCookieName').returns(true);

      let Dog = $peachAPI('dogs');
      sinon.assert.calledOnce(stub);
    });

    it('should get a new Object API while using the core key when saved keys are invalid format', () => {
      activeApp = 'myApp';
      MockCookieObject = {'myOtherApp': 'abc123'};

      let stub = sandbox.stub($peachSession, 'getSessionCookieName').returns(true);

      let Dog = $peachAPI('dogs');
      sinon.assert.calledOnce(stub);
    });

    it('should get a new Object API while using the core key, when useCoreToken is set', () => {
      activeApp = 'myApp';
      MockCookieObject = {'myOtherApp': 'abc123'};

      let stub = sandbox.stub($peachSession, 'getSessionCookieName').returns(true);

      let Dog = $peachAPI('dogs', {'useCoreToken': true});
      sinon.assert.calledOnce(stub);
    });

    describe('Create Object', () => {
      let createParams, response, rejectResponse;
      beforeEach(() => {
        createParams = {
          'name': 'Rover',
          'color': 'Brown'
        };

        response = {'message': 'Roll over, Rover!'};
        rejectResponse = {'message': 'Bad boy, Rover!'};
      });

      it('should get a promise from create', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let promise = $peachAPI('dogs').create(createParams);
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should create an object', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let obj;
        $peachAPI('dogs').create(createParams).then((returnFromPromise) => {
          obj = returnFromPromise;
        });

        $httpBackend.flush();
        expect(obj.message).to.equal(response.message);
      });

      it('should create an object collection', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let createParams2 = {
          'name': 'Frank',
          'color': 'White'
        };

        let obj;
        $peachAPI('dogs').create([createParams, createParams2]).then((returnFromPromise) => {
          obj = returnFromPromise;
        });

        $httpBackend.flush();
        expect(obj.message).to.equal(response.message);
      });

      it('should reject object creation', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(500, rejectResponse);

        let obj;
        $peachAPI('dogs').create(createParams).then((returnFromPromise) => {
          obj = returnFromPromise;
        }, (errorFromPromise) => {
          obj = errorFromPromise;
        });

        $httpBackend.flush();
        expect(obj.message).to.equal(rejectResponse.message);
      });

      it('should throw an error when creating a new object with bad bulk options', () => {
        expect(() => { $peachAPI('dogs').create(createParams, 1234); }).to.throw('CREATE options must be an object.');
      });

      it('should create an object collection with bulk options set', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let createParams2 = {
          'name': 'Frank',
          'color': 'White'
        };

        let bulkOptions = {
          verbose: false,
          atomic: true
        };

        let obj;
        $peachAPI('dogs').create([createParams, createParams2], bulkOptions).then((returnFromPromise) => {
          obj = returnFromPromise;
        });

        $httpBackend.flush();
        expect(obj.message).to.equal(response.message);
      });

      it('should create an object collection with query parameters', () => {
        $httpBackend.expectPOST(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?extended=true')
          .respond(200, response);

        let createParams2 = {
          'name': 'Frank',
          'color': 'White'
        };

        let bulkOptions = {
          extended:true,
          verbose: false,
          atomic: true
        };

        let obj;
        $peachAPI('dogs').create([createParams, createParams2], bulkOptions).then((returnFromPromise) => {
          obj = returnFromPromise;
        });

        $httpBackend.flush();
        expect(obj.message).to.equal(response.message);
      });
    });

    describe('Find Object', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = [{'id': 1, 'name': 'Rover','color': 'Brown'}, {'id': 2, 'name': 'Frank', 'color': 'White'}];
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should get a promise from find', () => {
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let promise = $peachAPI('dogs').find();
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should find all objects', () => {
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, response);

        let dogs;
        $peachAPI('dogs').find().then((returnFromPromise) => {
          dogs = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dogs.length).to.equal(2);
      });

      it('should error when finding objects', () => {
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(500, rejectResponse);

        let dogs;
        $peachAPI('dogs').find().then((returnFromPromise) => {
          dogs = returnFromPromise;
        }, (returnFromError) => {
          dogs = returnFromError;
        });

        $httpBackend.flush();
        expect(dogs.message).to.be.equal(rejectResponse.message);
      });

      it('should find object by id', () => {
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/2')
          .respond(200, response[1]);

        let dog;
        $peachAPI('dogs').find(2).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.name).to.be.equal('Frank');
      });

      it('should find object by id when id is a string', () => {
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/2')
          .respond(200, response[1]);

        let dog;
        $peachAPI('dogs').find('2').then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.name).to.be.equal('Frank');
      });

      it('should find object by query object', () => {
        let queryObj = {'color': 'Brown'};
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(queryObj)))
          .respond(200, response[0]);

        let dog;
        $peachAPI('dogs').find(queryObj).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.name).to.be.equal('Rover');
      });

      it('should find object by query object and search options', () => {
        let queryObj = {'color': 'Brown'};
        let searchOptions = {'sort': 'name', 'page': 1, 'limit': 25, 'fields': ['id', 'name']};
        $httpBackend.expectGET(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(queryObj)) + '&fields=id&fields=name&limit=25&page=1&sort=name')
          .respond(200, response[0]);

        let dog;
        $peachAPI('dogs').find(queryObj, searchOptions).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.name).to.be.equal('Rover');
      });
    });

    describe('Find and Remove Bulk Objects', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = [{'id': 1, 'name': 'Rover','color': 'Brown'}, {'id': 2, 'name': 'Frank', 'color': 'White'}];
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should get a promise from findAndRemove', () => {
        let deleteParams = {'color': 'Brown'};
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(deleteParams)))
          .respond(200, response);

        let promise = $peachAPI('dogs').findAndRemove(deleteParams);
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should throw an error if no query given to findAndRemove', () => {
        expect(() => { $peachAPI('dogs').findAndRemove(); }).to.throw('DELETE query must be an object.');
      });

      it('should throw an error if empty query given to findAndRemove', () => {
        expect(() => { $peachAPI('dogs').findAndRemove({}); }).to.throw('DELETE query must exist.');
      });

      it('should error when finding objects', () => {
        let deleteParams = {'color': 'Brown'};
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(deleteParams)))
          .respond(500, rejectResponse);

        let dogs;
        $peachAPI('dogs').findAndRemove(deleteParams).then((returnFromPromise) => {
          dogs = returnFromPromise;
        }, (returnFromError) => {
          dogs = returnFromError;
        });

        $httpBackend.flush();
        expect(dogs.message).to.be.equal(rejectResponse.message);
      });
    });

    describe('Find and Update Bulk Objects', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = [{'id': 1, 'name': 'Rover','color': 'Brown'}, {'id': 2, 'name': 'Frank', 'color': 'White'}];
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should get a promise from findAndUpdate', () => {
        let updateQuery = {'color': 'Brown'};
        let updateParams = {'color': 'Brownish'};
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(updateQuery)))
          .respond(200, response);

        let promise = $peachAPI('dogs').findAndUpdate(updateQuery, updateParams);
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should throw an error if no query given to findAndUpdate', () => {
        expect(() => { $peachAPI('dogs').findAndUpdate(); }).to.throw('PUT query must be an object.');
      });

      it('should throw an error if empty query given to findAndUpdate', () => {
        expect(() => { $peachAPI('dogs').findAndUpdate({}); }).to.throw('PUT query must exist.');
      });

      it('should throw an error if no query given to findAndUpdate', () => {
        expect(() => { $peachAPI('dogs').findAndUpdate({'color':'Brown'}); }).to.throw('PUT params must be an object.');
      });

      it('should throw an error if empty query given to findAndUpdate', () => {
        expect(() => { $peachAPI('dogs').findAndUpdate({'color':'Brown'}, {}); }).to.throw('PUT params must exist.');
      });

      it('should error when finding objects', () => {
        let updateQuery = {'color': 'Brown'};
        let updateParams = {'color': 'Brownish'};
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?find=' +
          encodeURIComponent(JSON.stringify(updateQuery)))
          .respond(500, rejectResponse);

        let dogs;
        $peachAPI('dogs').findAndUpdate(updateQuery, updateParams).then((returnFromPromise) => {
          dogs = returnFromPromise;
        }, (returnFromError) => {
          dogs = returnFromError;
        });

        $httpBackend.flush();
        expect(dogs.message).to.be.equal(rejectResponse.message);
      });
    });

    describe('Remove Object', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = {'message': 'The dogs have been removed'};
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should throw an error when removing an object with bad bulk options', () => {
        expect(() => { $peachAPI('dogs').remove(searchParams, 1234); }).to.throw('REMOVE options must be an object.');
      });

      it('should get a promise from remove', () => {
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/0')
          .respond(200, response);

        let promise = $peachAPI('dogs').remove(0);
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should error when removing objects', () => {
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/0')
          .respond(500, rejectResponse);

        let dogs;
        $peachAPI('dogs').remove(0).then((returnFromPromise) => {
          dogs = returnFromPromise;
        }, (returnFromError) => {
          dogs = returnFromError;
        });

        $httpBackend.flush();
        expect(dogs.message).to.be.equal(rejectResponse.message);
      });

      it('should remove object by id', () => {
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/2')
          .respond(200, response);

        let dog;
        $peachAPI('dogs').remove(2).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.message).to.be.equal(response.message);
      });

      it('should remove object by id when id is a string', () => {
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/2')
          .respond(200, response);

        let dog;
        $peachAPI('dogs').remove('2').then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.message).to.be.equal(response.message);
      });

      it('should remove object by object with id', () => {
        let deleteParams = {'id': 2, 'color': 'Brown'};
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/2')
          .respond(200, response);

        let dog;
        $peachAPI('dogs').remove(deleteParams).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.message).to.be.equal(response.message);
      });

      it('should throw an error if object does not have an id', () => {
        expect(() => { $peachAPI('dogs').remove({}); }).to.throw('DELETE object must have `id` property.');
      });

      it('should remove object by id array', () => {
        let queryArray = [1,2,3];
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').remove(queryArray).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(3);
      });

      it('should remove object by array of objects with id', () => {
        let queryArray = [{'id':1},{'id':2},{'id':3}];
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').remove(queryArray).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(3);
      });

      it('should remove object by array of mix of ids and objects with id', () => {
        let queryArray = [9, 8, {'id':1},{'id':2},{'id':3}, 7];
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').remove(queryArray).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(6);
      });

      it('should remove object by array of objects with id with atomic=false and verbose=true', () => {
        let queryArray = [{'id':1},{'id':2},{'id':3}];
        let removeOptions = {atomic: false, verbose: true};
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').remove(queryArray, removeOptions).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(3);
      });

      it('should set query parameters, and not include atomic or verbose', () => {
        let queryArray = [{'id':1},{'id':2},{'id':3}];
        let removeOptions = {atomic: false, verbose: true, extended: true};
        $httpBackend.expectDELETE(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?extended=true')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').remove(queryArray, removeOptions).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(3);
      });

      it('should throw an error if array is bad', () => {
        expect(() => { $peachAPI('dogs').remove([{}]); }).to.throw('DELETE array is malformed.');
      });

      it('should throw an error if query item is bad', () => {
        expect(() => { $peachAPI('dogs').remove(true); }).to.throw('DELETE array is malformed.');
      });
    });

    describe('Save Object', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = {'message': 'The dogs have been updated'};
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should get a promise from save', () => {
        let Dog = $peachAPI('dogs');
        sandbox.stub(Dog, 'create', () => { return true; });
        let promise = Dog.save({});
        expect(promise).not.to.equal(null);
      });

      it('should create an object if no id is present', () => {
        let Dog = $peachAPI('dogs');
        let stub = sandbox.stub(Dog, 'create', () => { return true; });
        Dog.save({'name': 'Randy'});
        sinon.assert.calledOnce(stub);
      });

      it('should update an object if an id is present', () => {
        let Dog = $peachAPI('dogs');
        let stub = sandbox.stub(Dog, 'update', () => { return true; });
        Dog.save({'name': 'Randy', 'id': 1});
        sinon.assert.calledOnce(stub);
      });

      it('should throw an error if not given an object', () => {
        expect(() => { $peachAPI('dogs').save(12); }).to.throw('SAVE properties must be an object.');
      });
    });

    describe('Update Object', () => {
      let searchParams, response, rejectResponse;
      beforeEach(() => {
        searchParams = {};

        response = {'message': 'The dogs have been updated'};
        rejectResponse = {'message': 'Found no dogs here.'};
      });

      it('should throw an error when removing an object with bad bulk options', () => {
        expect(() => { $peachAPI('dogs').update(searchParams, 1234); }).to.throw('UPDATE options must be an object.');
      });

      it('should get a promise from update', () => {
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/0')
          .respond(200, response);

        let promise = $peachAPI('dogs').update({'id': 0});
        $httpBackend.flush();
        expect(promise).not.to.equal(null);
      });

      it('should error when removing objects', () => {
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/0')
          .respond(500, rejectResponse);

        let dogs;
        $peachAPI('dogs').update({'id': 0}).then((returnFromPromise) => {
          dogs = returnFromPromise;
        }, (returnFromError) => {
          dogs = returnFromError;
        });

        $httpBackend.flush();
        expect(dogs.message).to.be.equal(rejectResponse.message);
      });

      it('should update an object', () => {
        let updateObj = {'id': 1, 'name': 'Charlie'};
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs/1')
          .respond(200, response);

          let dog;
          $peachAPI('dogs').update(updateObj).then((returnFromPromise) => {
            dog = returnFromPromise;
          });

          $httpBackend.flush();
          expect(dog.message).to.be.equal(response.message);
      });

      it('should update a `me` object', () => {
        let updateObj = {'id': 1, 'name': 'Charlie'};
        $httpBackend.expectPUT(apiURL+'/users/me')
          .respond(200, response);

          let dog;
          $peachAPI('/users/me').update(updateObj).then((returnFromPromise) => {
            dog = returnFromPromise;
          });

          $httpBackend.flush();
          expect(dog.message).to.be.equal(response.message);
      });

      it('should throw an error if object does not have an id', () => {
        expect(() => { $peachAPI('dogs').update({}); }).to.throw('PUT object must have `id` property.');
      });

      it('should update an array of objects', () => {
        let updateObj = [{'id': 1, 'name': 'Charlie'},{'id': 2, 'name': 'Sofie'}];
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond((method, url, data) => {
            data = angular.fromJson(data);
            expect(data.collection.length).to.be.equal(2);
            expect(data.collection[0].name).to.be.equal('Charlie');
            expect(data.collection[1].name).to.be.equal('Sofie');
            return [200, response];
          });

          let dog;
          $peachAPI('dogs').update(updateObj).then((returnFromPromise) => {
            dog = returnFromPromise;
          });

          $httpBackend.flush();
          expect(dog.message).to.be.equal(response.message);
      });

      it('should update object by array of mix of ids and objects with id', () => {
        let queryArray = [9, 8, {'id':1},{'id':2},{'id':3}, 7];
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs')
          .respond(200, queryArray);

        let dog;
        $peachAPI('dogs').update(queryArray).then((returnFromPromise) => {
          dog = returnFromPromise;
        });

        $httpBackend.flush();
        expect(dog.length).to.be.equal(6);
      });

      it('should update an array of objects with atomic=false and verbose=true', () => {
        let updateObj = [{'id': 1, 'name': 'Charlie'},{'id': 2, 'name': 'Sofie'}];
        let updateOptions = {atomic: false, verbose: true};
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs', (putData) => {
          let jsonData = angular.fromJson(putData);
          expect(jsonData.atomic).to.be.equal(false);
          expect(jsonData.verbose).to.be.equal(true);
          expect(jsonData.collection.length).to.be.equal(2);
          return true;
        }).respond(200, response);

          let dog;
          $peachAPI('dogs').update(updateObj, updateOptions).then((returnFromPromise) => {
            dog = returnFromPromise;
          });

          $httpBackend.flush();
          expect(dog.message).to.be.equal(response.message);
      });

      it('should set query parameters, and not include atomic or verbose', () => {
        let updateObj = [{'id': 1, 'name': 'Charlie'},{'id': 2, 'name': 'Sofie'}];
        let updateOptions = {atomic: false, verbose: true, extended: true};
        $httpBackend.expectPUT(apiURL+'/accounts/'+MockPeachSession.getActiveAccount()+'/dogs?extended=true')
          .respond(200, response);

          let dog;
          $peachAPI('dogs').update(updateObj, updateOptions).then((returnFromPromise) => {
            dog = returnFromPromise;
          });

          $httpBackend.flush();
          expect(dog.message).to.be.equal(response.message);
      });

      it('should throw an error if array is bad', () => {
        expect(() => { $peachAPI('dogs').update([{}]); }).to.throw('PUT array is malformed.');
      });

      it('should throw an error if query item is not an object or array of objects', () => {
        expect(() => { $peachAPI('dogs').update(12); }).to.throw('PUT properties must be an object or array of objects.');
      });
    });
  });
});
