/*jshint esnext: true */

import peachSession from '../session/session';

let moduleName = 'ngPeachApi';

class PeachAPIProvider {
  constructor() {
    let defaults = this.defaults = {
      useCoreToken: false
    };

    function PeachAPIService($http, $q, $cookies, $peachSession) {

      // https://github.com/peachworks/Peach-API/wiki/generic-objects

      function API($http, $q, $cookies, $peachSession, objectName, options) {
        let _self = this;
        let apiURL = '';
        let authToken = '';

        // Maintain alphabetical order of this list and their functions!
        let api = {};
        api.create = create;
        api.find = find;
        api.findAndRemove = findAndRemove;
        api.findAndUpdate = findAndUpdate;
        api.remove = remove;
        api.save = save;
        api.update = update;

        activate();

        return api;

        ///////////////

        function activate() {
          options = options || {};

          if (!angular.isObject(options)) {
            throw new Error('API options must be an object.');
          }

          apiURL = $peachSession.getApiUrl();
          options = _.merge(_.clone(defaults), options);

          // Override api path
          if(objectName.charAt(0) === '/') {
            apiURL += objectName;
          } else {
            apiURL += '/accounts/' + $peachSession.getActiveAccount() + '/' + objectName;
          }

          if($peachSession.isDevelopment() || $peachSession.isPreview()) {
            authToken = $cookies.get($peachSession.getDevSessionCookieName());
          } else {
            let activeApp = $peachSession.getActiveApp();
            let appKeys = $cookies.getObject($peachSession.getSessionAppTokensCookieName());
            if(options.useCoreToken || !_.has(appKeys, activeApp) || !_.has(appKeys[activeApp], 'access_token')) {
              authToken = $cookies.get($peachSession.getSessionCookieName());
            } else {
              authToken = appKeys[activeApp].access_token;
            }
          }
        }

        /**
         * Creates an object OR collection of objects
         *
         * Params
         *    properties - fields for the object OR array of objects to create
         *    options - Query parameter options
         *    bulkOptions - For multiple object creation, also combines with options
         *            - atomic - defaults to TRUE
         *            - verbose - defaults to TRUE
         */
        function create(properties, createOptions) {
          let deferred = $q.defer();
          let atomic = true;
          let verbose = false;

          createOptions = createOptions || {};

          if (!angular.isObject(createOptions)) {
            throw new Error('CREATE options must be an object.');
          }

          if(_.has(createOptions, 'atomic')) {
            atomic = createOptions.atomic;
            delete createOptions.atomic;
          }

          if(_.has(createOptions, 'verbose')) {
            verbose = createOptions.verbose;
            delete createOptions.verbose;
          }

          let req = {};
          req.method = 'POST';
          req.url = apiURL;
          req.headers = {
            'Content-Type': 'application/json',
            'Authorization': authToken
          };

          if(!_.isArray(properties)) {
            req.data = properties;
          } else {

            req.data = {
              atomic: atomic,
              verbose: verbose,
              collection: properties
            };
          }

          if(!_.isEmpty(createOptions)) {
            req.params = createOptions;
          }

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }

        /**
         * Finds a collection of objects.
         *
         * Params
         *    properties - null (returns all) OR id (returns object) OR find query (returns collection)
         *    options - query options object can include any of the following:
         *            - fields
         *            - includes
         *            - find
         *            - sort
         *            - page
         *            - limit
         */
        function find(properties, findOptions) {
          let deferred = $q.defer();

          let req = {};
          req.method = 'GET';
          req.url = apiURL;
          req.headers = {
            'Authorization': authToken
          };

          if(_.isNumber(properties) || _.isString(properties)) {
            req.url += '/' + properties;
          } else if(_.isObject(properties)) {
            // Angular still doesnt encode colons in objects when passing in through
            // req.params, so we have to do the encoding ourselves
            let escaped = encodeURIComponent(JSON.stringify(properties));
            req.url += '?find=' + escaped;
          }

          if(_.isObject(findOptions) && !_.isEmpty(findOptions)) {
            req.params = findOptions;
          }

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }

        /**
         * Finds objects and deletes them (only works for objects flagged as bulk_queryable)
         *
         * Params
         *    query - find query
         *    params - the fields and values to update
         */
        function findAndRemove(query, params) {
          let deferred = $q.defer();

          params = params || {};

          if (!angular.isObject(query)) {
            throw new Error('DELETE query must be an object.');
          }

          if (_.isEmpty(query)) {
            throw new Error('DELETE query must exist.');
          }

          let req = {};
          req.method = 'DELETE';
          req.url = apiURL;
          req.headers = {
            'Content-Type': 'application/json',
            'Authorization': authToken
          };
          req.data = params;

          // This is super ugly, and we should be able to use req.params, but:
          // https://github.com/angular/angular.js/issues/3740
          // Hopefully in 1.4 :(
          let escaped = encodeURIComponent(JSON.stringify(query));
          req.url += '?find=' + escaped;

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }

        /**
         * Finds objects and updates them (only works for objects flagged as bulk_queryable)
         *
         * Params
         *    query - find query
         *    params - the fields and values to update
         */
        function findAndUpdate(query, params) {
          let deferred = $q.defer();

          if (!angular.isObject(query)) {
            throw new Error('PUT query must be an object.');
          }

          if (_.isEmpty(query)) {
            throw new Error('PUT query must exist.');
          }

          if (!angular.isObject(params)) {
            throw new Error('PUT params must be an object.');
          }

          if (_.isEmpty(params)) {
            throw new Error('PUT params must exist.');
          }

          let req = {};
          req.method = 'PUT';
          req.url = apiURL;
          req.headers = {
            'Content-Type': 'application/json',
            'Authorization': authToken
          };
          req.data = params;

          // This is super ugly, and we should be able to use req.params, but:
          // https://github.com/angular/angular.js/issues/3740
          // Hopefully in 1.4 :(
          let escaped = encodeURIComponent(JSON.stringify(query));
          req.url += '?find=' + escaped;

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }

        /**
         * Deletes an object or collection of objects
         *
         * Params
         *    properties - object (with an "id" field) OR array of objects
         *    options - Query parameter options
         *    bulkOptions - For multiple object creation, also combines with options
         *            - atomic - defaults to TRUE
         *            - verbose - defaults to TRUE
         */
        function remove(properties, removeOptions) {
          let deferred = $q.defer();
          let atomic = true;
          let verbose = false;

          removeOptions = removeOptions || {};

          if (!angular.isObject(removeOptions)) {
            throw new Error('REMOVE options must be an object.');
          }

          if(_.has(removeOptions, 'atomic')) {
            atomic = removeOptions.atomic;
            delete removeOptions.atomic;
          }

          if(_.has(removeOptions, 'verbose')) {
            verbose = removeOptions.verbose;
            delete removeOptions.verbose;
          }

          let req = {};
          req.method = 'DELETE';
          req.url = apiURL;
          req.headers = {
            'Authorization': authToken
          };

          if(_.isNumber(properties) || _.isString(properties)) {
            req.url += '/' + properties;
          } else if(_.isArray(properties)) {
            let collection = [];

            for(let i = 0; i < properties.length; i++) {
              if(_.isNumber(properties[i]) || _.isString(properties[i])) {
                collection.push(properties[i]);
              } else if(_.isObject(properties[i]) && _.has(properties[i], 'id') && properties[i].id) {
                collection.push(properties[i].id);
              } else {
                throw new Error('DELETE array is malformed.');
              }
            }

            req.headers = {
              'Content-Type': 'application/json',
              'Authorization': authToken
            };

            req.data = {
              atomic: atomic,
              verbose: verbose,
              collection: collection
            };
          } else if(_.isObject(properties)) {
            if(!_.has(properties, 'id') && !properties.id) {
              throw new Error('DELETE object must have `id` property.');
            }

            req.url += '/' + properties.id;
          } else {
            throw new Error('DELETE array is malformed.');
          }

          if(!_.isEmpty(removeOptions)) {
            req.params = removeOptions;
          }

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }

        /**
         * Create object if no "id" param and update if an "id" param exists
         *
         * Params
         *    properties - object (with or without an "id" field)
         */
        function save(properties) {
          let deferred = $q.defer();

          if(_.isObject(properties)) {
            if(!_.has(properties, 'id') && !properties.id) {
              deferred.resolve(api.create(properties));
            } else {
              deferred.resolve(api.update(properties));
            }
          } else {
            throw new Error('SAVE properties must be an object.');
          }

          return deferred.promise;
        }

        /**
         * Updates an object or collection of objects
         *
         * Params
         *    properties - object (with an "id" field) or array of objects
         *    options - Query parameter options
         *    bulkOptions - For multiple object creation, also combines with options
         *            - atomic - defaults to TRUE
         *            - verbose - defaults to TRUE
         */
        function update(properties, updateOptions) {
          let deferred = $q.defer();
          let atomic = true;
          let verbose = false;

          updateOptions = updateOptions || {};

          if (!angular.isObject(updateOptions)) {
            throw new Error('UPDATE options must be an object.');
          }

          if(_.has(updateOptions, 'atomic')) {
            atomic = updateOptions.atomic;
            delete updateOptions.atomic;
          }

          if(_.has(updateOptions, 'verbose')) {
            verbose = updateOptions.verbose;
            delete updateOptions.verbose;
          }

          let req = {};
          req.method = 'PUT';
          req.url = apiURL;
          req.headers = {
            'Content-Type': 'application/json',
            'Authorization': authToken
          };

          if(_.isArray(properties)) {
            let collection = [];

            for(let i = 0; i < properties.length; i++) {
              if(_.isNumber(properties[i]) || _.isString(properties[i])) {
                collection.push(properties[i]);
              } else if(_.isObject(properties[i]) && _.has(properties[i], 'id') && properties[i].id) {
                collection.push(properties[i]);
              } else {
                throw new Error('PUT array is malformed.');
              }
            }

            req.data = {
              atomic: atomic,
              verbose: verbose,
              collection: collection
            };
          } else if(_.isObject(properties)) {

            if(req.url.indexOf('/me', req.url.length - 3) === -1) {
              if(!_.has(properties, 'id') && !properties.id) {
                throw new Error('PUT object must have `id` property.');
              }

              req.url += '/' + properties.id;
              delete properties.id;
              delete properties.created_at;
              delete properties.created_by;
              delete properties.is_deleted;
              delete properties.updated_at;
              delete properties.updated_by;
              delete properties.uri;
            }

            req.data = properties;
          } else {
            throw new Error('PUT properties must be an object or array of objects.');
          }

          if(!_.isEmpty(updateOptions)) {
            req.params = updateOptions;
          }

          $http(req)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((data) => {
              deferred.reject(data);
            });

          return deferred.promise;
        }
      }

      return function(objectName, options) {
        return new API($http, $q, $cookies, $peachSession, objectName, options);
      };
    }


    this.$get = ($http, $q, $cookies, $peachSession) => {
      return new PeachAPIService($http, $q, $cookies, $peachSession);
    };

    this.$get.$inject = ['$http', '$q', '$cookies', '$peachSession'];
  }
}

PeachAPIProvider.$inject = [];

angular.module(moduleName, [
  peachSession.moduleName
])
  .provider('$peachApi', PeachAPIProvider);

export default {moduleName, provider: PeachAPIProvider};
