/*jshint esnext: true */

let moduleName = 'ngPeachEvent';

class PeachEventProvider {
  constructor() {
    let defaults = this.defaults = {};

    function PeachEventService($rootScope, $log) {
      let _self = this;

      // Maintain alphabetical order of this list and their functions!
      let service = {};
      service.events = {};
      service.publish = publish;
      service.subscribe = subscribe;

      activate();

      return service;

      ///////////////

      function activate() {
        service.events = {
          APP_CHANGE: 'appChangeEvent',
          HIDE_INTRO_PAGE: 'hideIntroPageEvent',
          LOCATION_CHANGE: 'locationChangeEvent',
          PAGE_CHANGE: 'pageChangeEvent',
          REFRESH_APP_LIST: 'refreshAppListEvent'
        };
      }

      function publish(eventName, data) {
        $rootScope.$emit(eventName, data);
      }

      function subscribe(eventName, handler) {
        return $rootScope.$on(eventName, (e, data) => {
          handler(data);
        });
      }
    }


    this.$get = function($rootScope, $log) {
      return new PeachEventService($rootScope, $log);
    };

    this.$get.$inject = ['$rootScope', '$log'];
  }
}

PeachEventProvider.$inject = [];

angular.module(moduleName, [])
  .provider('$peachEvent', PeachEventProvider);

export default {moduleName, provider: PeachEventProvider};
