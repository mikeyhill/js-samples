/*jshint esnext: true */

import PeachEvent from './event';

describe('Provider: Peach Event', () => {
  let provider, sandbox, $rootScope, $log;

  beforeEach(angular.mock.module('ng', ($provide) => {

  }));

  beforeEach(inject((_$rootScope_, _$log_) => {
    $rootScope = _$rootScope_;
    $log = _$log_;

    provider = new PeachEvent.provider();

    sandbox = sinon.sandbox.create();
  }));

  afterEach(() => {
    sandbox.restore();
  });

  it('should construct a Peach Event Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachEvent.provider(); }).to.throw('Cannot call a class as a function');
  });

  describe('Service: Peach Event', () => {
    let $peachEvent;

    beforeEach(() => {
      $peachEvent = provider.$get($rootScope, $log);
    });

    it('should construct a $peachEvent service', () => {
      expect($peachEvent).not.to.equal('undefined');
    });

    it('should let you publish to and subscribe to an event', () => {
      let test = null;
      $peachEvent.subscribe('MyEvent', (data) => {
        test = data;
      });

      $peachEvent.publish('MyEvent', 1);

      expect(test).to.be.equal(1);
    });
  });
});
