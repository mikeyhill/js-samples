/*jshint esnext: true */

import PeachUser from './user';
import ngPeach from '../../ngpeach';

describe('Provider: Peach User', () => {
  let provider, $rootScope, $q, $peachSession, $peachApp, $peachApi, sandbox, cacheData,
      MockCacheFactory, MockCache, MockPeachAPI, MockPeachApp, MockPeachSession,
      activeApp, activeAppInfo, isCoreApp, isDevelopment, isPreview, activeAccount, mockFindResults,
      activeAppPromise, apiFindPromise, apiCreatePromise, apiUpdatePromise;

  beforeEach(angular.mock.module(ngPeach, ($provide) => {

    MockPeachAPI = () => {
      return (objectName) => {
        return {
          find: () => {
            return apiFindPromise;
          },
          create: () => {
            return apiCreatePromise;
          },
          update: () => {
            return apiUpdatePromise;
          }
        };
      };
    };

    MockPeachApp = () => {
      return {
        getInfo: () => {
          return activeAppPromise;
        },
        isCoreApp: () => {
          return isCoreApp;
        }
      };
    };

    MockPeachSession = () => {
      return {
        isDevelopment: () => {
          return isDevelopment;
        },
        isPreview: () => {
          return isPreview;
        },
        getActiveApp: () => {
          return activeApp;
        },
        getActiveAccount: () => {
          return activeAccount;
        },
        getDevSessionCookieName: () => {
          return 'dev_cookie';
        },
        getSessionCookieName: () => {
          return 'session_cookie';
        },
        getApiUrl: () => {
          return 'http://my.api.com/v1';
        },
        getApiVersion: () => {
          return 'v1';
        }
      };
    };

    MockCache = {
      get: (key) => {
        return cacheData;
      },
      put: (key, value) => {
        return true;
      },
      remove: (key) => {
        return true;
      },
      disable: () => {
        return true;
      }
    };

    MockCacheFactory = {
      get: (cacheId) => {
        return MockCache;
      },
      createCache: (cacheId, options) => {
        return MockCache;
      }
    };

    $provide.factory('CacheFactory', MockCacheFactory);
    $provide.factory('$peachApp', MockPeachApp);
    $provide.factory('$peachApi', MockPeachAPI);
    $provide.factory('$peachSession', MockPeachSession);
  }));


  beforeEach(inject((_$rootScope_, _$q_, _$peachSession_, _$peachApp_, _$peachApi_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $peachSession = _$peachSession_;
    $peachApp = _$peachApp_;
    $peachApi = _$peachApi_;
    provider = new PeachUser.provider();

    sandbox = sinon.sandbox.create();

    cacheData = {'user': {'name': 'Test User'}, 'accounts': [{'name': 'Test Account 1'}]};

    mockFindResults = {'user': {'id': 1, 'name': 'testUser'}};
    activeAccount = 1;
    isDevelopment = false;
    isPreview = false;
    isCoreApp = false;
    activeApp = 'test_app';
    activeAppInfo = {'id': '1234', 'name': 'Test App', 'key': 'test_app'};
  }));

  afterEach(() => {
    sandbox.restore();
  });

  it('should construct a Peach User Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachUser.provider(); }).to.throw('Cannot call a class as a function');
  });

  it('should construct $peachUser service with new cache', () => {
    MockCache = {disable: () => { return true; }};
    let $peachUser = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    expect($peachUser).to.be.an('object');
  });

  it('should construct $peachUser service with existing cache', () => {
    let $peachUser = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    expect($peachUser).to.be.an('object');
  });

  describe('Service: Peach User', () => {
    let $peachUser;

    beforeEach(() => {
      $peachUser = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    });

    it('should construct a $peachUser service', () => {
      expect($peachUser).not.to.equal('undefined');
    });

    ////////////////////////////////////////////////////////////////////////////
    // getAccounts
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getAccounts', () => {
      let promise = $peachUser.getAccounts();
      expect(promise).not.to.equal(null);
    });

    it('should get accounts from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 1}, {'id': 2}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAccounts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(2);
      expect(ret[0].id).to.equal(mockFindResults.results[0].id);
    });

    it('should get handle reject get accounts from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAccounts().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get accounts from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachUser.getAccounts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(2);
      expect(ret[0].id).to.equal(cacheData[0].id);
    });

    it('should not return accounts when in development or preview mode', () => {
      isDevelopment = true;
      isPreview = true;
      mockFindResults = null;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAccounts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getAlertsPrefs
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getAlertsPrefs', () => {
      let promise = $peachUser.getAlertsPrefs();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get alert prefs without an active account', () => {
      activeAccount = null;

      let ret;
      $peachUser.getAlertsPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all user alert prefs for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachUser.getAlertsPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get alert pref by key for active account from cache', () => {
      cacheData = [{'alert_id': 2, 'key': 'myPref', 'value': true}, {'alert_id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachUser.getAlertsPrefs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for alert pref by non-existant key for active account from cache', () => {
      cacheData = [{'alert_id': 2, 'key': 'myPref', 'value': true}, {'alert_id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachUser.getAlertsPrefs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get alert prefs from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAlertsPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings alert prefs from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAlertsPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single alert pref from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'alert_id': 2, 'key': 'myPref', 'value': true}, {'alert_id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAlertsPrefs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all alert prefs from API but return null if specific pref does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'alert_id': 2, 'key': 'myPref', 'value': true}, {'alert_id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getAlertsPrefs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getInfo
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getInfo', () => {
      let promise = $peachUser.getInfo();
      expect(promise).not.to.equal(null);
    });

    it('should get user from API', () => {
      cacheData = null;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let user;
      $peachUser.getInfo().then((returnFromPromise) => {
        user = returnFromPromise;
      });
      $rootScope.$apply();

      expect(user.name).to.equal(mockFindResults.user.name);
    });

    it('should get handle reject get user from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let user;
      $peachUser.getInfo().then((returnFromPromise) => {
        user = returnFromPromise;
      }, (returnFromError) => {
        user = returnFromError;
      });
      $rootScope.$apply();

      expect(user.message).to.equal(mockFindResults.message);
    });

    it('should get user from cache', () => {
      let deferred = $q.defer();
      deferred.resolve(cacheData);

      sandbox.stub($peachUser, 'getInfo').returns(deferred.promise);

      let user;
      $peachUser.getInfo().then((returnFromPromise) => {
        user = returnFromPromise;
      });
      $rootScope.$apply();
      expect(user).to.equal(cacheData);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getPrefs
    ////////////////////////////////////////////////////////////////////////////

    it('should get all prefs for active account from cache', () => {
      cacheData = [{'myPref': true, 'app_id': activeAppInfo.id}];

      let deferred = $q.defer();
      deferred.resolve(activeAppInfo);
      activeAppPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get all prefs from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when trying to get all prefs from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single pref from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs('myPref').then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all prefs from API but return null if specific pref does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs('myPrefDoesntExist').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get all prefs for core app', () => {
      cacheData = null;
      isCoreApp = true;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': 1}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs(null, true).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should throw an error if no active account found', () => {
      cacheData = null;
      activeAccount = null;
      mockFindResults = {'message': 'Error!'};

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should throw an error if no active app found', () => {
      cacheData = null;
      activeApp = null;
      mockFindResults = {'message': 'Error!'};

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });

    it('should get pref by key for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': activeAppInfo.id}, {'key': 'myOtherPref', 'id': 888}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachUser.getPrefs('myPref').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.id).to.equal(cacheData[0].id);
    });

    it('should get empty results for pref by non-existant key for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': '999'}, {'key': 'myOtherPref', 'id': 888}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachUser.getPrefs('prefIDontHave').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get pref from core app for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': 1}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];
      isCoreApp = true;

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachUser.getPrefs(null, true).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should handle error with getPrefs when peachApp.getInfo has an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.reject(mockFindResults);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });


    ////////////////////////////////////////////////////////////////////////////
    // hasConfig
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from hasConfig', () => {
      let promise = $peachUser.hasConfig();
      expect(promise).not.to.equal(null);
    });

    it('should get truthy hasConfig', () => {
      isDevelopment = true;
      let deferred = $q.defer();
      deferred.resolve(true);

      sandbox.stub($peachUser, 'hasCorePermission').returns(deferred.promise);

      let ret;
      $peachUser.hasConfig().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should get falsy hasConfig', () => {
      let deferred = $q.defer();
      deferred.reject(false);

      sandbox.stub($peachUser, 'hasCorePermission').returns(deferred.promise);

      let ret;
      $peachUser.hasConfig().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should handle errors when getting is user hasConfig', () => {
      activeAccount = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(false);

      sandbox.stub($peachUser, 'hasCorePermission').returns(deferred.promise);

      let ret;
      $peachUser.hasConfig().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    ////////////////////////////////////////////////////////////////////////////
    // hasReports
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from hasReports', () => {
      let promise = $peachUser.hasReports();
      expect(promise).not.to.equal(null);
    });

    it('hasReports should throw an error if no active account found', () => {
      cacheData = null;
      activeAccount = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasReports().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should return true if user has reports from API', () => {
      cacheData = null;
      mockFindResults = {'count': 7};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasReports().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return false if user has no reports from API', () => {
      cacheData = null;
      mockFindResults = {'count': 0};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasReports().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should handle errors when getting reports from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(false);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasReports().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    ////////////////////////////////////////////////////////////////////////////
    // hasCorePermission
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from hasCorePermission', () => {
      let promise = $peachUser.hasCorePermission();
      expect(promise).not.to.equal(null);
    });

    it('should get if user has core permission from cache', () => {
      cacheData = [{'key': 'myPermission'}];

      let ret;
      $peachUser.hasCorePermission('myPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return false if user does not have core permission from cache', () => {
      cacheData = [{'key': 'myPermission'}];

      let ret;
      $peachUser.hasCorePermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should return true if user has core permission from API', () => {
      cacheData = null;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasCorePermission('myPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return false if user does not have core permission from API', () => {
      cacheData = null;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasCorePermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('core permissions check should return false if API returns an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(false);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasCorePermission().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should return true for core permissions check if user is in development preview mode', () => {
      cacheData = null;
      isDevelopment = true;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.hasCorePermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return error for core permissions check if no active account', () => {
      activeAccount = null;

      let ret;
      $peachUser.hasCorePermission().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    ////////////////////////////////////////////////////////////////////////////
    // setAlertPref
    ////////////////////////////////////////////////////////////////////////////

    it('should update an existing alert pref with setAlertPref', () => {
      cacheData = [{'id': 1, 'alert_id': 100, 'key': 'value'}, {'id': 2, 'alert_id': 200, 'key': 'value2'}];
      mockFindResults = {'id': 1, 'alert_id': 100, 'key': 'value3'};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiUpdatePromise = deferred.promise;

      let ret;
      $peachUser.setAlertPref(cacheData[0].alert_id, cacheData[0]).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.value).to.equal(mockFindResults.value);
    });

    it('should create a new pref with setAlertPref', () => {
      cacheData = [{'id': 1, 'alert_id': 100, 'key': 'value'}, {'id': 2, 'alert_id': 200, 'key': 'value2'}];
      mockFindResults = {'id': 3, 'alert_id': 300, 'key': 'value3'};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiCreatePromise = deferred.promise;

      let ret;
      $peachUser.setAlertPref(cacheData[0].id, cacheData[0]).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.id).to.equal(mockFindResults.id);
    });

    it('should handle error with setAlertPref', () => {
      cacheData = [{'id': 1, 'alert_id': 100, 'key': 'value'}, {'id': 2, 'alert_id': 200, 'key': 'value2'}];
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiCreatePromise = deferred.promise;

      let ret;
      $peachUser.setAlertPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should handle error with setAlertPref when getAlertPref has an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.setAlertPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    ////////////////////////////////////////////////////////////////////////////
    // setPref
    ////////////////////////////////////////////////////////////////////////////

    it('should update an existing pref with setPref', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': activeAppInfo.id}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiUpdatePromise = deferred.promise;

      let ret;
      $peachUser.setPref('myPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.value).to.equal(mockFindResults.value);
    });

    it('should create a new pref with setPref', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': 1}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];
      mockFindResults = {'key': 'someRandomPref', 'value': 'myVal'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiCreatePromise = deferred.promise;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.value).to.equal(mockFindResults.value);
    });

    it('should create a new core pref with setPref', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': 1}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];
      mockFindResults = {'key': 'someRandomPref', 'value': 'myVal'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiCreatePromise = deferred.promise;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal', true).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.value).to.equal(mockFindResults.value);
    });

    it('should handle error with setPref', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': 1}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiCreatePromise = deferred.promise;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should handle error with setPref when getPref has an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should return error for setPref check if no active account', () => {
      activeAccount = null;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should return error for setPref check if no active app', () => {
      activeApp = null;

      let ret;
      $peachUser.setPref('someRandomPref', 'myVal').then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });

    ////////////////////////////////////////////////////////////////////////////
    // setInfo
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from setInfo', () => {
      let promise = $peachUser.setInfo();
      expect(promise).not.to.equal(null);
    });

    it('should setInfo for the current user', () => {
      mockFindResults = {'user': {'name': 'Test User'}};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiUpdatePromise = deferred.promise;

      let ret;
      $peachUser.setInfo({'name': 'Test User'}).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.user.name).to.equal(mockFindResults.user.name);
    });

    it('should handle errors for setInfo for the current user', () => {
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiUpdatePromise = deferred.promise;

      let ret;
      $peachUser.setInfo({'name': 'Test User'}).then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });
  });
});
