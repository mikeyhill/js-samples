/*jshint esnext: true */

import peachAPI     from '../api/api';
import peachApp     from '../app/app';
import peachSession from '../session/session';

let moduleName = 'ngPeachUser';

class PeachUserProvider {
  constructor() {
    let defaults = this.defaults = {};

    function PeachUserService($q, CacheFactory, $peachSession, $peachApp, $peachApi) {
      let _self = this;
      let cache = null;
      let cacheName = 'userCache';

      // Maintain alphabetical order of this list and their functions!
      let service = {};
      service.getAccounts = getAccounts;
      service.getAlertsPrefs = getAlertsPrefs;
      service.getInfo = getInfo;
      service.getPrefs = getPrefs;
      service.hasConfig = hasConfig;
      service.hasCorePermission = hasCorePermission;
      service.hasReports = hasReports;
      service.setAlertPref = setAlertPref;
      service.setInfo = setInfo;
      service.setPref = setPref;

      activate();

      return service;

      ///////////////

      function activate() {
        if (!CacheFactory.get(cacheName)) {
          cache = CacheFactory.createCache(cacheName, {
            maxAge: 1 * 24 * 60 * 60 * 1000, // 1 days
            deleteOnExpire: 'aggressive',
            storageMode: 'memory'
          });
        } else {
          cache = CacheFactory.get(cacheName);
        }

        cache.disable();
      }

      function getAccounts() {
        let deferred = $q.defer();
        if($peachSession.isDevelopment() || $peachSession.isPreview()) {
          deferred.resolve(null);
          return deferred.promise;
        }

        if(cache.get('accounts')) {
          deferred.resolve(cache.get('accounts'));
        } else {
          $peachApi('/accounts', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('accounts', data.results);
              deferred.resolve(data.results);
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getAlertsPrefs(alert_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up alert preferences."}});
          return deferred.promise;
        }

        let cachedPrefs = cache.get('/alert_prefs/' + activeAccount);

        if(cachedPrefs) {
          if(alert_id) {
            let pref = _.where(cachedPrefs, {'alert_id': alert_id})[0];
            if(pref) {
              deferred.resolve(pref);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cachedPrefs);
          }
        } else {
          $peachApi('user_alert_prefs', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/alert_prefs/' + activeAccount, data.results);

              if(alert_id) {
                let pref = _.find(data.results, {'alert_id': alert_id});

                if(pref) {
                  deferred.resolve(pref);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getInfo() {
        let deferred = $q.defer();
        if(cache.get('user')) {
          deferred.resolve(cache.get('user'));
        } else {
          $peachApi('/users/me/simplified', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('user', data.user);
              deferred.resolve(data.user);
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getPrefs(key, isCore) {
        let deferred = $q.defer();
        let coreDeferred = $q.defer();
        isCore = (_.isUndefined(isCore)) ? $peachApp.isCoreApp() : isCore;
        let appPromise = (isCore) ? coreDeferred.promise : $peachApp.getInfo();
        let activeAccount = $peachSession.getActiveAccount();
        let activeApp = (isCore) ? 'core' : $peachSession.getActiveApp();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        if(!activeApp) {
          deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        if(isCore) {
          coreDeferred.resolve({id: 1});
        }

        appPromise
          .then((appInfo) => {
            let activeAppId = appInfo.id;

            let cachedPrefs = cache.get('/prefs/' + activeAccount + '/' + activeApp);

            if(cachedPrefs) {
              cachedPrefs = _.where(cachedPrefs, {'app_id': activeAppId});

              if(key) {
                let pref = _.find(cachedPrefs, {'key': key});
                if(pref) {
                  deferred.resolve(pref);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(cachedPrefs);
              }
            } else {
              $peachApi('user_prefs', {useCoreToken: true}).find({'app_id': activeAppId})
                .then((data) => {
                  cache.put('/prefs/' + activeAccount + '/' + activeApp, data.results);
                  data.results = _.where(data.results, {'app_id': activeAppId});

                  if(key) {
                    let pref = _.find(data.results, {'key': key});

                    if(pref) {
                      deferred.resolve(pref);
                    } else {
                      deferred.resolve(null);
                    }
                  } else {
                    deferred.resolve(data.results);
                  }
                }, (error) => {
                  deferred.reject(error);
                });
            }
          }, (error) => {
            deferred.reject(error);
          });

        return deferred.promise;
      }

      function hasConfig() {
        let deferred = $q.defer();

        let promises = [];
        promises.push(hasCorePermission('manage_users'));
        promises.push(hasCorePermission('assign_roles'));
        promises.push(hasCorePermission('assign_locations'));
        promises.push(hasCorePermission('manage_locations'));
        promises.push(hasCorePermission('manage_app_locations'));
        promises.push(hasCorePermission('admin_manage_roles'));
        promises.push(hasCorePermission('show_day_parts_settings_page'));
        promises.push(hasCorePermission('show_gl_accounts_settings_page'));

        $q.all(promises).then((data) => {
          let ret = false;
          _.forIn(data, (value) => {
            if(value) {
              ret = true;
            }
          });
          deferred.resolve(ret);
        }, (error) => {
          deferred.resolve(false);
        });

        return deferred.promise;
      }

      function hasCorePermission(key) {
        let deferred = $q.defer();
        let isDeveloper = $peachSession.isDevelopment() || $peachSession.isPreview();

        if(isDeveloper) {
          deferred.resolve(true);
          return deferred.promise;
        }

        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up permissions."}});
          return deferred.promise;
        }

        let cached = cache.get('core_permissions');

        if(cached) {
          let permission = _.find(cached, {'key': key});

          if(permission) {
            deferred.resolve(!!permission);
          } else {
            deferred.resolve(false);
          }
        } else {
          $peachApi('apps/core/permissions', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('core_permissions', data.permissions);
              let permission = _.find(data.permissions, {'key': key});

              if(permission) {
                deferred.resolve(!!permission);
              } else {
                deferred.resolve(false);
              }
            }, (error) => {
              deferred.resolve(false);
            });
        }

        return deferred.promise;
      }

      function hasReports() {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up reports."}});
          return deferred.promise;
        }

        $peachApi('reports', {useCoreToken: true}).find(null, {
          'limit': 0
        })
          .then((data) => {
            deferred.resolve(data.count > 0);
          }, (error) => {
            deferred.reject(error);
          });

        return deferred.promise;
      }

      function setAlertPref(alert_id, alertPrefs) {
        let deferred = $q.defer();

        getAlertsPrefs(alert_id)
          .then((data) => {
            let prefs = $peachApi('user_alert_prefs', {useCoreToken: true});

            if(_.isEmpty(data)) {
              //alertPrefs.alert_id = alert_id;
              prefs = prefs.create(alertPrefs);
            } else {
              _.merge(data, alertPrefs);
              prefs = prefs.update(data);
            }

            prefs.then((data) => {
              cache.remove('/alert_prefs/' + $peachSession.getActiveAccount());
              getAlertsPrefs();
              deferred.resolve(data);
            }, (error) => {
              deferred.reject(error);
            });
          }, (error) => {
            deferred.reject(error);
          });
        return deferred.promise;
      }

      function setInfo(user) {
        let deferred = $q.defer();

        let api = $peachApi('/users/me', {useCoreToken: true}).update(user)
          .then((data) => {
            cache.remove('user');
            getInfo();
            deferred.resolve(data);
          }, (error) => {
            deferred.reject(error);
          });

        return deferred.promise;
      }

      function setPref(key, value, isCore) {
        let deferred = $q.defer();
        isCore = (_.isUndefined(isCore)) ? $peachApp.isCoreApp() : isCore;
        let activeAccount = $peachSession.getActiveAccount();
        let activeApp = (isCore) ? 'core' : $peachSession.getActiveApp();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        if(!activeApp) {
          deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        let promises = [];
        promises.push(getPrefs(key, isCore));
        promises.push($peachApp.getInfo());

        $q.all(promises).then((data) => {
          let prefs = data[0];
          let app = data[1];
          let activeAppId = (isCore) ? 1 : app.id;

          let prefObj = $peachApi('user_prefs', {useCoreToken: true});

          if(_.isEmpty(prefs)) {
            prefObj = prefObj.create({
              'app_id': activeAppId,
              'key': key,
              'value': value
            });
          } else {
            prefObj = prefObj.update({
              'id': prefs.id,
              'value': value
            });
          }

          prefObj.then((data) => {
            cache.remove('/prefs/' + activeAccount + '/' + activeApp);
            getPrefs(null, isCore);
            deferred.resolve(data);
          }, (error) => {
            deferred.reject(error);
          });
        }, (error) => {
          deferred.reject(error);
        });

        return deferred.promise;
      }
    }


    this.$get = function($q, CacheFactory, $peachSession, $peachApp, $peachApi) {
      return new PeachUserService($q, CacheFactory, $peachSession, $peachApp, $peachApi);
    };

    this.$get.$inject = ['$q', 'CacheFactory', '$peachSession', '$peachApp', '$peachApi'];
  }
}

PeachUserProvider.$inject = [];

angular.module(moduleName, [
  peachAPI.moduleName,
  peachApp.moduleName,
  peachSession.moduleName,
])
  .provider('$peachUser', PeachUserProvider);

export default {moduleName, provider: PeachUserProvider};
