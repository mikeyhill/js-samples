/*jshint esnext: true */

import peachAPI     from '../api/api';
import peachApp     from '../app/app';
import peachSession from '../session/session';

let moduleName = 'ngPeachAccount';

class PeachAccountProvider {
  constructor() {
    let defaults = this.defaults = {};

    function PeachAccountService($q, CacheFactory, $peachSession, $peachApp, $peachApi) {
      let _self = this;
      let cache = null;
      let cacheName = 'accountCache';

      // Maintain alphabetical order of this list and their functions!
      let service = {};
      service.getAlerts = getAlerts;
      service.getDayParts = getDayParts;
      service.getDocs = getDocs;
      service.getEmployees = getEmployees;
      service.getInfo = getInfo;
      service.getJobs = getJobs;
      service.getLocations = getLocations;
      service.getPrefs = getPrefs;
      service.getRoles = getRoles;
      service.getUsers = getUsers;

      activate();

      return service;

      ///////////////

      function activate() {
        if (!CacheFactory.get(cacheName)) {
          cache = CacheFactory.createCache(cacheName, {
            maxAge: 1 * 24 * 60 * 60 * 1000, // 1 days
            deleteOnExpire: 'aggressive',
            storageMode: 'memory'
          });
        } else {
          cache = CacheFactory.get(cacheName);
        }

        cache.disable();
      }

      function getAlerts(alert_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up alerts."}});
          return deferred.promise;
        }

        let cached = cache.get('/alerts/' + activeAccount);

        if(cached) {
          if(alert_id) {
            let alert = _.find(cached, {'id': alert_id});

            if(alert) {
              deferred.resolve(alert);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('alerts', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/alerts/' + activeAccount, data.results);

              if(alert_id) {
                let alert = _.find(data.results, {'id': alert_id});

                if(alert) {
                  deferred.resolve(alert);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              //temp solution until API is smart enough to return empty set instead of 401 error for users without permission to alerts
              deferred.resolve([]);
              //deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getDayParts(day_part_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up day parts."}});
          return deferred.promise;
        }

        let cached = cache.get('/day_parts/' + activeAccount);

        if(cached) {
          if(day_part_id) {
            let dayPart = _.find(cached, {'id': day_part_id});

            if(dayPart) {
              deferred.resolve(dayPart);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('day_parts', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/day_parts/' + activeAccount, data.results);

              if(day_part_id) {
                let dayPart = _.find(data.results, {'id': day_part_id});

                if(dayPart) {
                  deferred.resolve(dayPart);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getDocs(doc_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up docs."}});
          return deferred.promise;
        }

        let cached = cache.get('/docs/' + activeAccount);

        if(cached) {
          if(doc_id) {
            let doc = _.find(cached, {'id': doc_id});

            if(doc) {
              deferred.resolve(doc);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('docs', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/docs/' + activeAccount, data.results);

              if(doc_id) {
                let doc = _.find(data.results, {'id': doc_id});

                if(doc) {
                  deferred.resolve(doc);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              //temp solution until API is smart enough to return empty set instead of 401 error for users without permission to docs
              deferred.resolve([]);
              //deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getEmployees(employee_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up employees."}});
          return deferred.promise;
        }

        let cached = cache.get('/employees/' + activeAccount);

        if(cached) {
          if(employee_id) {
            let employee = _.find(cached, {'id': employee_id});

            if(employee) {
              deferred.resolve(employee);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('employees', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/employees/' + activeAccount, data.results);

              if(employee_id) {
                let employee = _.find(data.results, {'id': employee_id});

                if(employee) {
                  deferred.resolve(employee);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getInfo() {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();
        let isDeveloper = $peachSession.isDevelopment() || $peachSession.isPreview();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up information."}});
          return deferred.promise;
        }

        let apiPath = '/accounts/' + activeAccount;
        let cacheKey = '/account/' + activeAccount;

        if(isDeveloper) {
          apiPath = '/developers/me';
          cacheKey = '/dev_account/' + activeAccount;
        }

        let cached = cache.get(cacheKey);

        if(cached) {
          deferred.resolve(cached);
        } else {
          $peachApi(apiPath, {useCoreToken: true}).find()
            .then((data) => {
              if(isDeveloper) {
                let account = _.find(data.developer_accounts, {'id': activeAccount});

                if(account) {
                  cache.put(cacheKey);
                  deferred.resolve(account);
                } else {
                  deferred.resolve(null);
                }
              } else {
                cache.put(cacheKey);
                deferred.resolve(data);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getJobs(job_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up jobs."}});
          return deferred.promise;
        }

        let cached = cache.get('/jobs/' + activeAccount);

        if(cached) {
          if(job_id) {
            let job = _.find(cached, {'id': job_id});

            if(job) {
              deferred.resolve(job);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('jobs', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/jobs/' + activeAccount, data.results);

              if(job_id) {
                let job = _.find(data.results, {'id': job_id});

                if(job) {
                  deferred.resolve(job);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getLocations(location_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up locations."}});
          return deferred.promise;
        }

        let cached = cache.get('/locations/' + activeAccount);

        if(cached) {
          if(location_id) {
            let location = _.find(cached, {'id': location_id});

            if(location) {
              deferred.resolve(location);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('locations/me', {useCoreToken: true}).find(null, {'sort': 'name'})
            .then((data) => {
              cache.put('/locations/' + activeAccount, data.results);

              if(location_id) {
                let location = _.find(data.results, {'id': location_id});

                if(location) {
                  deferred.resolve(location);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getPrefs(key, isCore) {
        let deferred = $q.defer();
        let coreDeferred = $q.defer();
        isCore = (_.isUndefined(isCore)) ? $peachApp.isCoreApp() : isCore;
        let appPromise = (isCore) ? coreDeferred.promise : $peachApp.getInfo();
        let activeAccount = $peachSession.getActiveAccount();
        let activeApp = (isCore) ? 'core' : $peachSession.getActiveApp();
        let isDeveloper = $peachSession.isDevelopment() || $peachSession.isPreview();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        if(!activeApp) {
          deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up preferences."}});
          return deferred.promise;
        }

        if(isCore) {
          coreDeferred.resolve({id: 1});
        }

        appPromise
          .then((appInfo) => {
            let activeAppId = appInfo.id;

            let apiPath = 'account_prefs';
            let cacheKey = '/prefs/' + activeAccount + '/' + activeApp;

            if(isDeveloper) {
              apiPath = '/developers/' + activeAccount + '/account_prefs';
              cacheKey = '/dev_prefs/' + activeAccount + '/' + activeApp;
            }

            let cachedPrefs = cache.get(cacheKey);

            if(cachedPrefs) {
              if(isCore) {
                cachedPrefs = _.where(cachedPrefs, {'app_id': activeAppId});
              }

              if(key) {
                let pref = _.find(cachedPrefs, {'key': key});
                if(pref) {
                  deferred.resolve(pref);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(cachedPrefs);
              }
            } else {
              $peachApi(apiPath, {useCoreToken: true}).find({'app_id': activeAppId})
                .then((data) => {
                  cache.put(cacheKey, data.results);
                  if(isCore) {
                    data.results = _.where(data.results, {'app_id': activeAppId});
                  }

                  if(key) {
                    let pref = _.find(data.results, {'key': key});

                    if(pref) {
                      deferred.resolve(pref);
                    } else {
                      deferred.resolve(null);
                    }
                  } else {
                    deferred.resolve(data.results);
                  }
                }, (error) => {
                  deferred.reject(error);
                });
            }
          }, (error) => {
            deferred.reject(error);
          });

        return deferred.promise;
      }

      function getRoles(role_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up roles."}});
          return deferred.promise;
        }

        let cached = cache.get('/roles/' + activeAccount);

        if(cached) {
          if(role_id) {
            let role = _.find(cached, {'id': role_id});

            if(role) {
              deferred.resolve(role);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('roles', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/roles/' + activeAccount, data.results);

              if(role_id) {
                let role = _.find(data.results, {'id': role_id});

                if(role) {
                  deferred.resolve(role);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getUsers(user_id) {
        let deferred = $q.defer();
        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up users."}});
          return deferred.promise;
        }

        let cached = cache.get('/users/' + activeAccount);

        if(cached) {
          if(user_id) {
            let user = _.find(cached, {'id': user_id});

            if(user) {
              deferred.resolve(user);
            } else {
              deferred.resolve(null);
            }
          } else {
            deferred.resolve(cached);
          }
        } else {
          $peachApi('users', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/users/' + activeAccount, data.results);

              if(user_id) {
                let user = _.find(data.results, {'id': user_id});

                if(user) {
                  deferred.resolve(user);
                } else {
                  deferred.resolve(null);
                }
              } else {
                deferred.resolve(data.results);
              }
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }
    }


    this.$get = function($q, CacheFactory, $peachSession, $peachApp, $peachApi) {
      return new PeachAccountService($q, CacheFactory, $peachSession, $peachApp, $peachApi);
    };

    this.$get.$inject = ['$q', 'CacheFactory', '$peachSession', '$peachApp', '$peachApi'];
  }
}

PeachAccountProvider.$inject = [];

angular.module(moduleName, [
  peachAPI.moduleName,
  peachApp.moduleName,
  peachSession.moduleName,
])
  .provider('$peachAccount', PeachAccountProvider);


export default {moduleName, provider: PeachAccountProvider};
