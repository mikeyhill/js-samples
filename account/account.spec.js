/*jshint esnext: true */

import PeachAccount from './account';

describe('Provider: Peach Account', () => {
  let provider, sandbox, $rootScope, $q, $peachSession, $peachApp, $peachApi,
  MockCacheFactory, MockCache, MockPeachAPI, MockPeachApp, MockPeachSession,
  activeApp, activeAppInfo, isCoreApp, cacheData, activeAccount, isDevelopment, isDeveloper,
  isPreview, activeAppPromise, apiFindPromise, apiCreatePromise, apiUpdatePromise, mockFindResults;

  beforeEach(angular.mock.module('ng', ($provide) => {

    MockPeachAPI = () => {
      return (objectName) => {
        return {
          find: () => {
            return apiFindPromise;
          },
          create: () => {
            return apiCreatePromise;
          },
          update: () => {
            return apiUpdatePromise;
          }
        };
      };
    };

    MockPeachApp = () => {
      return {
        getInfo: () => {
          return activeAppPromise;
        },
        isCoreApp: () => {
          return isCoreApp;
        }
      };
    };

    MockPeachSession = () => {
      return {
        isDevelopment: () => {
          return isDevelopment;
        },
        isPreview: () => {
          return isPreview;
        },
        getActiveAccount: () => {
          return activeAccount;
        },
        getActiveApp: () => {
          return activeApp;
        },
        getApiUrl: () => {
          return 'http://my.api.com/v1';
        }
      };
    };

    MockCache = {
      get: (key) => {
        return cacheData;
      },
      put: (key, value) => {
        return true;
      },
      disable: () => {
        return true;
      }
    };

    MockCacheFactory = {
      get: (cacheId) => {
        return MockCache;
      },
      createCache: (cacheId, options) => {
        return MockCache;
      },
      destroyAll: () => {
        return true;
      }
    };

    $provide.factory('CacheFactory', MockCacheFactory);
    $provide.factory('$peachApp', MockPeachApp);
    $provide.factory('$peachApi', MockPeachAPI);
    $provide.factory('$peachSession', MockPeachSession);
  }));

  beforeEach(inject((_$rootScope_, _$q_, _$peachSession_, _$peachApp_, _$peachApi_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $peachSession = _$peachSession_;
    $peachApp = _$peachApp_;
    $peachApi = _$peachApi_;
    provider = new PeachAccount.provider();

    sandbox = sinon.sandbox.create();

    cacheData = [{'id': '0'}, {'id': '1'}];
    activeAccount = 1;
    isDevelopment = false;
    isPreview = false;
    isCoreApp = false;
    activeApp = 'test_app';
    activeAppInfo = {'id': '1234', 'name': 'Test App', 'key': 'test_app'};
  }));

  afterEach(() => {
    sandbox.restore();
  });

  it('should construct a Peach Account Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachAccount.provider(); }).to.throw('Cannot call a class as a function');
  });

  it('should construct $peachAccount service with new cache', () => {
    MockCache = {disable: () => { return true; }};
    let $peachAccount = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    expect($peachAccount).to.be.an('object');
  });

  it('should construct $peachAccount service with existing cache', () => {
    let $peachAccount = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    expect($peachAccount).to.be.an('object');
  });

  describe('Service: Peach Account', () => {
    let $peachAccount;

    beforeEach(() => {
      $peachAccount = provider.$get($q, MockCacheFactory, $peachSession, $peachApp, $peachApi);
    });

    it('should construct a $peachAccount service', () => {
      expect($peachAccount).not.to.equal('undefined');
    });

    ////////////////////////////////////////////////////////////////////////////
    // getAlerts
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getAlerts', () => {
      let promise = $peachAccount.getAlerts(999);
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get alerts without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getAlerts().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all alerts for active account from cache', () => {
      cacheData = [{'app_id': 2, 'name': 'Alert 1', 'id': 1}];

      let ret;
      $peachAccount.getAlerts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get alerts by id for active account from cache', () => {
      cacheData = [{'app_id': 2, 'name': 'Alert 1', 'id': 1}, {'app_id': 3, 'name': 'Alert 2', 'id': 2}];

      let ret;
      $peachAccount.getAlerts(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for alert by non-existant id for active account from cache', () => {
      cacheData = [{'app_id': 2, 'name': 'Alert 1', 'id': 1}, {'app_id': 3, 'name': 'Alert 2', 'id': 2}];

      let ret;
      $peachAccount.getAlerts(9).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get alerts from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'app_id': 2, 'name': 'Alert 1', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getAlerts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when getting alerts from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getAlerts().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret).to.deep.equals([]);
    });

    it('should get a single alert from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'app_id': 2, 'name': 'Alert 1', 'id': 1}, {'app_id': 3, 'name': 'Alert 2', 'id': 2}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getAlerts(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all alerts from API but return null if specific alert does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'app_id': 2, 'name': 'Alert 1', 'id': 1}, {'app_id': 3, 'name': 'Alert 2', 'id': 2}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getAlerts(3).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getDayParts
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getDayParts', () => {
      let promise = $peachAccount.getDayParts();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get day parts without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getDayParts().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all day parts for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getDayParts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get day part by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getDayParts(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for day part by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getDayParts(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get day parts from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDayParts().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings day parts from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDayParts().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single day part from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDayParts(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all day parts from API but return null if specific part does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDayParts(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getDocs
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getDocs', () => {
      let promise = $peachAccount.getDocs(999);
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get docs without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getDocs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all docs for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getDocs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get docs by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getDocs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for doc by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getDocs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get docs from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDocs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings docs from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDocs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      //expect(ret.message).to.equal(mockFindResults.message);
      expect(ret).to.deep.equals([]);
    });

    it('should get a single doc from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDocs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all docs from API but return null if specific location does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getDocs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getEmployees
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getEmployees', () => {
      let promise = $peachAccount.getEmployees();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get employees without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getEmployees().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all employees for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getEmployees().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get employee by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getEmployees(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for employee by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getEmployees(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get employees from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getEmployees().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings employees from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getEmployees().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single employee from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getEmployees(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all employees from API but return null if specific employee does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getEmployees(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getInfo
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getInfo', () => {
      let promise = $peachAccount.getInfo();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get info without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get info for active account from cache', () => {
      cacheData = {'id': 1, 'name': 'Test Account'};

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.id).to.equal(cacheData.id);
    });

    it('should get account info from API', () => {
      cacheData = null;
      mockFindResults = {'id': 1, 'name': 'Test Account'};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.id).to.equal(mockFindResults.id);
    });

    it('should handle errors when getting account info from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get developer account info from API', () => {
      cacheData = null;
      isDeveloper = true;
      isPreview = true;
      activeAccount = 1;
      mockFindResults = {'developer_accounts': [{'id': 1, 'name': 'Test Account'}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.name).to.equal(mockFindResults.developer_accounts[0].name);
    });

    it('should return null if unable to get developer account info from API', () => {
      cacheData = null;
      isDeveloper = true;
      isPreview = true;
      activeAccount = 1;
      mockFindResults = {'developer_accounts': [{'id': 2, 'name': 'Test Account'}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should handle errors when getting developer account info from API', () => {
      cacheData = null;
      isDeveloper = true;
      isPreview = true;
      activeAccount = 1;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getJobs
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getJobs', () => {
      let promise = $peachAccount.getJobs();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get jobs without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getJobs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all jobs for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getJobs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get job by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getJobs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for job by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getJobs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get jobs from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getJobs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings jobs from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getJobs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single job from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getJobs(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all jobs from API but return null if specific job does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getJobs(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getLocations
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getLocations', () => {
      let promise = $peachAccount.getLocations(999);
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get locations without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getLocations().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all locations for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getLocations().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get location by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getLocations(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for location by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getLocations(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get locations from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getLocations().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings locations from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getLocations().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single location from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getLocations(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all locations from API but return null if specific location does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getLocations(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getPrefs
    ////////////////////////////////////////////////////////////////////////////

    it('should get all prefs for active account from cache', () => {
      cacheData = [{'myPref': true, 'app_id': activeAppInfo.id}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get all prefs from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should get all prefs for developer account from API', () => {
      cacheData = null;
      isDeveloper = true;
      isPreview = true;
      mockFindResults = {'results': [{'myPref': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when trying to get all prefs from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single pref from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs('myPref').then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all prefs from API but return null if specific pref does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': activeAppInfo.id}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs('myPrefDoesntExist').then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

    it('should get all prefs for core app', () => {
      cacheData = null;
      mockFindResults = {'results': [{'key': 'myPref', 'value': true, 'app_id': 1}]};

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs(null, true).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should throw an error if no active account found', () => {
      cacheData = null;
      activeAccount = null;
      mockFindResults = {'message': 'Error!'};

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should throw an error if no active app found', () => {
      cacheData = null;
      activeApp = null;
      mockFindResults = {'message': 'Error!'};

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.error.name).to.equal('No Active Application');
    });

    it('should get pref by key for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': activeAppInfo.id}, {'key': 'myOtherPref', 'id': 888}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachAccount.getPrefs('myPref').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.id).to.equal(cacheData[0].id);
    });

    it('should get empty results for pref by non-existant key for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': 999}, {'key': 'myOtherPref', 'id': 888}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachAccount.getPrefs('prefIDontHave').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get pref from core app for active account from cache', () => {
      cacheData = [{'key': 'myPref', 'id': 999, 'app_id': 1}, {'key': 'myOtherPref', 'id': 888, 'app_id': 2}];

      let appDeferred = $q.defer();
      appDeferred.resolve(activeAppInfo);
      activeAppPromise = appDeferred.promise;

      let ret;
      $peachAccount.getPrefs(null, true).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should handle error with getPrefs when peachApp.getInfo has an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let appDeferred = $q.defer();
      appDeferred.reject(mockFindResults);
      activeAppPromise = appDeferred.promise;

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getPrefs().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });



    ////////////////////////////////////////////////////////////////////////////
    // getJobs
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getRoles', () => {
      let promise = $peachAccount.getRoles();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get jobs without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getRoles().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all roles for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getRoles().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get role by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getRoles(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for role by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getRoles(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get roles from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getRoles().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings roles from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getRoles().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single role from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getRoles(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all roles from API but return null if specific role does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getRoles(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });


    ////////////////////////////////////////////////////////////////////////////
    // getUsers
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getUsers', () => {
      let promise = $peachAccount.getUsers();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get users without an active account', () => {
      activeAccount = null;

      let ret;
      $peachAccount.getUsers().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all users for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachAccount.getUsers().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get user by id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getUsers(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(cacheData[0].key);
    });

    it('should get empty results for user by non-existant id for active account from cache', () => {
      cacheData = [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}];

      let ret;
      $peachAccount.getUsers(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(null);
    });

    it('should get users from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getUsers().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings users from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getUsers().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get a single user from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getUsers(2).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should load all users from API but return null if specific user does not exist', () => {
      cacheData = null;
      mockFindResults = {'results': [{'id': 2, 'key': 'myPref', 'value': true}, {'id': 9, 'key': 'myPref', 'value': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachAccount.getUsers(1).then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret).to.equal(null);
    });

  });
});
