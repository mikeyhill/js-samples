/*jshint esnext: true */

import peachAPI     from '../api/api';
import peachSession from '../session/session';

let moduleName = 'ngPeachApp';

class PeachAppProvider {
  constructor() {
    let defaults = this.defaults = {};

    function PeachAppService($q, $route, CacheFactory, $peachSession, $peachApi) {
      let _self = this;
      let cache = null;
      let cacheName = 'appCache';

      // Maintain alphabetical order of this list and their functions!
      let service = {};
      service.getAllApps = getAllApps;
      service.getAppPages = getAppPages;
      service.getInfo = getInfo;
      service.getSettingsPages = getSettingsPages;
      service.hasPermission = hasPermission;
      service.isCoreApp = isCoreApp;

      activate();

      return service;

      ///////////////

      function activate() {
        if (!CacheFactory.get(cacheName)) {
          cache = CacheFactory.createCache(cacheName, {
            maxAge: 1 * 24 * 60 * 60 * 1000, // 1 days
            deleteOnExpire: 'aggressive',
            storageMode: 'memory'
          });
        } else {
          cache = CacheFactory.get(cacheName);
        }

        cache.disable();
      }

      function getAllApps() {
        let deferred = $q.defer();
        let isDeveloper = $peachSession.isDevelopment() || $peachSession.isPreview();

        if(isDeveloper) {
          $peachApi('/apps', {useCoreToken: true}).find({is_deleted: false})
            .then((data) => {
              _.remove(data.results, {'key': 'reports'});
              deferred.resolve(data.results);
            }, (error) => {
              deferred.reject(error);
            });

          return deferred.promise;
        }

        let activeAccount = $peachSession.getActiveAccount();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up account apps."}});
          return deferred.promise;
        }

        let cached = cache.get('/apps/' + activeAccount);

        if(cached) {
          deferred.resolve(cached);
        } else {
          $peachApi('apps', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/apps/' + activeAccount, data.results);
              deferred.resolve(data.results);
            }, (error) => {
              deferred.reject(error);
            });
        }

        return deferred.promise;
      }

      function getAppPages() {
        let deferred = $q.defer();

        if(isCoreApp() || $peachSession.isDevelopment()) {
          let pages = [];
          _.forIn($route.routes, function(value, key) {
            if(_.has(value, 'name') && (!_.has(value, 'is_settings_page') || !value.is_settings_page)) {
              pages.push(value);
            }
          });
          deferred.resolve(pages);
        } else {

          let activeAccount = $peachSession.getActiveAccount();
          let activeApp = $peachSession.getActiveApp();

          if(!activeAccount) {
            deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up pages."}});
            return deferred.promise;
          }

          if(!activeApp) {
            deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up pages."}});
            return deferred.promise;
          }

          let cached = cache.get('/pages/' + activeAccount + '/' + activeApp);

          if(cached) {
            deferred.resolve(cached);
          } else {
            $peachApi('apps/' + activeApp + '/pages', {useCoreToken: true}).find(null, {sort: 'position'})
              .then((data) => {
                let pages = _.where(data.results, {'is_settings_page': false});
                cache.put('/pages/' + activeAccount + '/' + activeApp, pages);
                deferred.resolve(pages);
              }, (error) => {
                deferred.reject(error);
              });
          }
        }

        return deferred.promise;
      }

      function getInfo() {
        let deferred = $q.defer();
        let activeApp = isCoreApp() ? 1 : $peachSession.getActiveApp();

        if(!activeApp) {
          deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up information."}});
          return deferred.promise;
        }

        getAllApps()
          .then((data) => {
            deferred.resolve(_.find(data, {'key': activeApp}));
          }, (error) => {
            deferred.reject(error);
          });

        return deferred.promise;
      }

      function getSettingsPages() {
        let deferred = $q.defer();
        if(isCoreApp() || $peachSession.isDevelopment()) {
          let pages = [];
          _.forIn($route.routes, function(value, key) {
            if(_.has(value, 'name') && _.has(value, 'is_settings_page') && value.is_settings_page) {
              pages.push(value);
            }
          });
          deferred.resolve(pages);
        } else {

          let activeAccount = $peachSession.getActiveAccount();
          let activeApp = $peachSession.getActiveApp();

          if(!activeAccount) {
            deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up settings pages."}});
            return deferred.promise;
          }

          if(!activeApp) {
            deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up settings pages."}});
            return deferred.promise;
          }

          let cached = cache.get('/settings_pages/' + activeAccount + '/' + activeApp);

          if(cached) {
            deferred.resolve(cached);
          } else {
            $peachApi('apps/' + activeApp + '/pages', {useCoreToken: true}).find()
              .then((data) => {
                let pages = _.where(data.results, {'is_settings_page': true});
                cache.put('/settings_pages/' + activeAccount + '/' + activeApp, pages);
                deferred.resolve(pages);
              }, (error) => {
                deferred.reject(error);
              });
          }
        }

        return deferred.promise;
      }

      function hasPermission(key) {
        let deferred = $q.defer();

        if($peachSession.isDevelopment()) {
          deferred.resolve(true);
          return deferred.promise;
        }

        let activeAccount = $peachSession.getActiveAccount();
        let activeApp = (isCoreApp()) ? 1 : $peachSession.getActiveApp();

        if(!activeAccount) {
          deferred.reject({"error":{"name":"No Active Account", "message":"An active account is not found, so we are unable to look up permissions."}});
          return deferred.promise;
        }

        if(!activeApp) {
          deferred.reject({"error":{"name":"No Active Application", "message":"An active application is not found, so we are unable to look up permissions."}});
          return deferred.promise;
        }

        let cached = cache.get('/permissions/' + activeAccount + '/' + activeApp);

        if(cached) {
          let permission = _.find(cached, {'key': key});

          if(permission) {
            deferred.resolve(!!permission);
          } else {
            deferred.resolve(false);
          }
        } else {
          $peachApi('apps/' + activeApp + '/permissions', {useCoreToken: true}).find()
            .then((data) => {
              cache.put('/permissions/' + activeAccount + '/' + activeApp, data.permissions);
              let permission = _.find(data.permissions, {'key': key});

              if(permission) {
                deferred.resolve(!!permission);
              } else {
                deferred.resolve(false);
              }
            }, (error) => {
              deferred.resolve(false);
            });
        }

        return deferred.promise;
      }

      function isCoreApp() {
        let coreApp = false;
        let activeApp = $peachSession.getActiveApp();

        if(activeApp === 'welcome' || activeApp === 'dashboard' ||
          activeApp === 'reports' || activeApp === 'profile' ||
          activeApp === 'help' || activeApp === 'config') {
          coreApp = true;
        }

        return coreApp;
      }
    }


    this.$get = function($q, $route, CacheFactory, $peachSession, $peachApi) {
      return new PeachAppService($q, $route, CacheFactory, $peachSession, $peachApi);
    };

    this.$get.$inject = ['$q', '$route', 'CacheFactory', '$peachSession', '$peachApi'];
  }
}

PeachAppProvider.$inject = [];

angular.module(moduleName, [
  peachAPI.moduleName,
  peachSession.moduleName,
])
  .provider('$peachApp', PeachAppProvider);

export default {moduleName, provider: PeachAppProvider};
