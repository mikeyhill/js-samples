/*jshint esnext: true */

import PeachApp from './app';

describe('Provider: Peach App', () => {
  let provider, sandbox, $rootScope, $q, $peachSession, $peachApi, $route,
  MockCacheFactory, MockCache, MockPeachAPI, MockPeachSession,
  cacheData, activeAccount, activeApp, isDevelopment, isPreview,
  apiFindPromise, apiCreatePromise, apiUpdatePromise, mockFindResults;

  beforeEach(angular.mock.module('ng', ($provide) => {

    let MockRoute = () => {
      return {
        'routes': [
          {
            'id': 1,
            'name': 'Home'
          }, {
            'id': 2,
            'name': 'Main',
            'is_settings_page': false
          }, {
            'id': 3,
            'name': 'Settings',
            'is_settings_page': true
          }
        ]
      };
    };

    MockPeachAPI = () => {
      return (objectName) => {
        return {
          find: () => {
            return apiFindPromise;
          },
          create: () => {
            return apiCreatePromise;
          },
          update: () => {
            return apiUpdatePromise;
          }
        };
      };
    };

    MockPeachSession = () => {
      return {
        isDevelopment: () => {
          return isDevelopment;
        },
        isPreview: () => {
          return isPreview;
        },
        getActiveAccount: () => {
          return activeAccount;
        },
        getActiveApp: () => {
          return activeApp;
        },
        getApiUrl: () => {
          return 'http://my.api.com/v1';
        }
      };
    };

    MockCache = {
      get: (key) => {
        return cacheData;
      },
      put: (key, value) => {
        return true;
      },
      disable: () => {
        return true;
      }
    };

    MockCacheFactory = {
      get: (cacheId) => {
        return MockCache;
      },
      createCache: (cacheId, options) => {
        return MockCache;
      },
      destroyAll: () => {
        return true;
      }
    };

    $provide.factory('CacheFactory', MockCacheFactory);
    $provide.factory('$peachApi', MockPeachAPI);
    $provide.factory('$peachSession', MockPeachSession);
    $provide.factory('$route', MockRoute);
  }));

  beforeEach(inject((_$rootScope_, _$q_, _$route_, _$peachSession_, _$peachApi_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $route = _$route_;
    $peachSession = _$peachSession_;
    $peachApi = _$peachApi_;
    provider = new PeachApp.provider();

    sandbox = sinon.sandbox.create();

    cacheData = [{'id': '0'}, {'id': '1'}];
    activeAccount = 1;
    activeApp = 'myApp';
    isDevelopment = false;
    isPreview = false;
  }));

  afterEach(() => {
    sandbox.restore();
  });

  it('should construct a Peach App Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachApp.provider(); }).to.throw('Cannot call a class as a function');
  });

  it('should construct $peachApp service with new cache', () => {
    MockCache = {disable: () => { return true; }};
    let $peachApp = provider.$get($q, $route, MockCacheFactory, $peachSession, $peachApi);
    expect($peachApp).to.be.an('object');
  });

  it('should construct $peachApp service with existing cache', () => {
    let $peachApp = provider.$get($q, $route, MockCacheFactory, $peachSession, $peachApi);
    expect($peachApp).to.be.an('object');
  });

  describe('Service: Peach App', () => {
    let $peachApp;

    beforeEach(() => {
      $peachApp = provider.$get($q, $route, MockCacheFactory, $peachSession, $peachApi);
    });

    it('should construct a $peachApp service', () => {
      expect($peachApp).not.to.equal('undefined');
    });

    ////////////////////////////////////////////////////////////////////////////
    // getAllApps
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getAllApps', () => {
      let promise = $peachApp.getAllApps();
      expect(promise).not.to.equal(null);
    });

    it('should handle errors when user wants to get all apps without an active account', () => {
      activeAccount = null;

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should get all apps for active account from cache', () => {
      cacheData = [{'myPref': true}];

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(cacheData[0].myPref);
    });

    it('should get apps from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'myPref': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(1);
      expect(ret[0].myPref).to.equal(mockFindResults.results[0].myPref);
    });

    it('should handle errors when gettings apps from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    it('should get development apps from API', () => {
      cacheData = null;
      isDevelopment = true;
      activeApp = 'test_app';
      mockFindResults = {'results': [{'key': 'test_app'}, {'key': 'test_app2'}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      });
      $rootScope.$apply();

      expect(ret.length).to.equal(2);
    });

    it('should handle errors when getting development apps from API', () => {
      cacheData = null;
      isDevelopment = true;
      activeApp = 'test_app';
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAllApps().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });
      $rootScope.$apply();

      expect(ret.message).to.equal(mockFindResults.message);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getAppPages
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getAppPages', () => {
      let promise = $peachApp.getAppPages();
      expect(promise).not.to.equal(null);
    });

    it('should get pages from the router if in development mode', () => {
      isDevelopment = true;

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(2);
    });

    it('should return error for page lookup if no active account', () => {
      activeAccount = null;

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should return error for page lookup if no active app', () => {
      activeApp = null;

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });

    it('should get app pages from cache', () => {
      cacheData = [{'name': 'Page'}];

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret[0].name).to.equal(cacheData[0].name);
    });

    it('should get app pages from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'name': 'Page', 'id': 1, 'is_settings_page': false}, {'name': 'Settings', 'id': 2, 'is_settings_page': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].name).to.equal(mockFindResults.results[0].name);
    });

    it('should handle errors when getting app pages from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getAppPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    ////////////////////////////////////////////////////////////////////////////
    // getInfo
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getInfo', () => {
      let promise = $peachApp.getInfo();
      expect(promise).not.to.equal(null);
    });

    it('should getInfo for the current app', () => {
      cacheData = [{'key': 'app'}, {'key': activeApp}];

      let ret;
      $peachApp.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(activeApp);
    });

    it('should getInfo for the core app', () => {
      activeApp = 'welcome';
      cacheData = [{'key': 'app'}, {'key': 1}];

      let ret;
      $peachApp.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.key).to.equal(1);
    });

    it('should handle error for getInfo for the current app', () => {
      activeAccount = null;
      cacheData = [{'key': 'app'}, {'key': activeApp}];

      let ret;
      $peachApp.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should handle error for getInfo if no app is active', () => {
      activeApp = null;
      cacheData = [{'key': 'app'}, {'key': activeApp}];

      let ret;
      $peachApp.getInfo().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });

    ////////////////////////////////////////////////////////////////////////////
    // getSettingsPages
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from getSettingsPages', () => {
      let promise = $peachApp.getSettingsPages();
      expect(promise).not.to.equal(null);
    });

    it('should get settings pages from the router if in development mode', () => {
      isDevelopment = true;

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
    });

    it('should get settings pages from the router if a core app', () => {
      activeApp = 'welcome';

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
    });

    it('should return error for settings page lookup if no active account', () => {
      activeAccount = null;

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should return error for settings page lookup if no active app', () => {
      activeApp = null;

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });

    it('should get settings pages from cache', () => {
      cacheData = [{'name': 'Page', 'is_settings_page': true}];

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret[0].name).to.equal(cacheData[0].name);
    });

    it('should get settings pages from API', () => {
      cacheData = null;
      mockFindResults = {'results': [{'name': 'Page', 'id': 1, 'is_settings_page': false}, {'name': 'Settings', 'id': 2, 'is_settings_page': true}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret.length).to.equal(1);
      expect(ret[0].name).to.equal(mockFindResults.results[1].name);
    });

    it('should handle errors when getting settings pages from API', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.getSettingsPages().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.message).to.equal(mockFindResults.message);
    });

    ////////////////////////////////////////////////////////////////////////////
    // hasPermission
    ////////////////////////////////////////////////////////////////////////////

    it('should get a promise from hasPermission', () => {
      let promise = $peachApp.hasPermission();
      expect(promise).not.to.equal(null);
    });

    it('should get if user has permission from cache', () => {
      cacheData = [{'key': 'myPermission'}];

      let ret;
      $peachApp.hasPermission('myPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should get if user has permission for core app from cache', () => {
      activeApp = 'welcome';
      cacheData = [{'key': 'myPermission'}];

      let ret;
      $peachApp.hasPermission('myPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return false if user does not have permission from cache', () => {
      cacheData = [{'key': 'myPermission'}];

      let ret;
      $peachApp.hasPermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should return true if user has permission from API', () => {
      cacheData = null;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.hasPermission('myPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return false if user does not have permission from API', () => {
      cacheData = null;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.hasPermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('permissions check should return false if API returns an error', () => {
      cacheData = null;
      mockFindResults = {'message': 'Error!'};

      let deferred = $q.defer();
      deferred.reject(false);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.hasPermission().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret).to.equal(false);
    });

    it('should return true for permissions check if user is in development preview mode', () => {
      cacheData = null;
      isDevelopment = true;
      mockFindResults = {'permissions': [{'key': 'myPermission', 'id': 1}]};

      let deferred = $q.defer();
      deferred.resolve(mockFindResults);
      apiFindPromise = deferred.promise;

      let ret;
      $peachApp.hasPermission('myOtherPermission').then((returnFromPromise) => {
        ret = returnFromPromise;
      });

      $rootScope.$apply();
      expect(ret).to.equal(true);
    });

    it('should return error for permissions check if no active account', () => {
      activeAccount = null;

      let ret;
      $peachApp.hasPermission().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Account');
    });

    it('should return error for permissions check if no active app', () => {
      activeApp = null;

      let ret;
      $peachApp.hasPermission().then((returnFromPromise) => {
        ret = returnFromPromise;
      }, (returnFromError) => {
        ret = returnFromError;
      });

      $rootScope.$apply();
      expect(ret.error.name).to.equal('No Active Application');
    });


    ////////////////////////////////////////////////////////////////////////////
    // isActiveApp
    ////////////////////////////////////////////////////////////////////////////
    it('should return true for isCoreApp "welcome"', () => {
      activeApp = 'welcome';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return true for isCoreApp "dashboard"', () => {
      activeApp = 'dashboard';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return true for isCoreApp "reports"', () => {
      activeApp = 'reports';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return true for isCoreApp "profile"', () => {
      activeApp = 'profile';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return true for isCoreApp "help"', () => {
      activeApp = 'help';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return true for isCoreApp "config"', () => {
      activeApp = 'config';
      expect($peachApp.isCoreApp()).to.equal(true);
    });

    it('should return false for isCoreApp and all other apps', () => {
      activeApp = 'someApp';
      expect($peachApp.isCoreApp()).to.equal(false);
    });
  });
});
