/*jshint esnext: true */

let moduleName = 'ngPeachInterceptorAPI';

class PeachInterceptorAPIProvider {
  constructor() {

    function PeachInterceptorAPIFactory($injector, $q, $window) {

      // Maintain alphabetical order of this list and their functions!
      let factory = {};
      factory.request = request;
      factory.requestError = requestError;
      factory.response = response;
      factory.responseError = responseError;

      return factory;

      ///////////////

      function request(config) {
        /*let $peach = $injector.get('$peach');

        if(_.startsWith(config.url, $peach.session.getApiUrl())) {
          // Perform API specific handler
        }*/

        return config;
      }

      function requestError(rejection) {
        // Uncomment this scaffold when we actually put some data here, otherwise
        // its a couple wasted operations
        /*let $peach = $injector.get('$peach');

        if(_.startsWith(config.url, $peach.session.getApiUrl())) {
          // Perform API specific handler
        }*/

        return $q.reject(rejection);
      }

      function response(data) {
        // Uncomment this scaffold when we actually put some data here, otherwise
        // its a couple wasted operations
        /*let $peach = $injector.get('$peach');

        if(_.startsWith(config.url, $peach.session.getApiUrl())) {
          // Perform API specific handler
        }*/

        return data;
      }

      function responseError(rejection) {
        let $peach = $injector.get('$peach');

        if(_.startsWith(rejection.config.url, $peach.session.getApiUrl())) {
          // Perform API specific handler
          if(rejection.config.url === $peach.session.getApiUrl() + '/oauth/session/logout') {
            return $q.reject(rejection);
          } else if(rejection.config.url === $peach.session.getApiUrl() + '/users/password/reset') {
            return $q.reject(rejection);
          }

          switch(rejection.status) {
            case 401: {
              if($window.parent.location.pathname !== '/' && $window.parent.location.pathname !== '/login') {
                $peach.session.logout();
                $window.parent.location.href = '/';
              }
              break;
            }
          }
        }

        return $q.reject(rejection);
      }
    }


    this.$get = function($injector, $q, $window) {
      return new PeachInterceptorAPIFactory($injector, $q, $window);
    };

    this.$get.$inject = ['$injector', '$q', '$window'];
  }
}

PeachInterceptorAPIProvider.$inject = [];

angular.module(moduleName, [])
  .provider('peachInterceptorAPI', PeachInterceptorAPIProvider);

export default {moduleName, provider: PeachInterceptorAPIProvider};
