/*jshint esnext: true */

import PeachInterceptorAPI from './interceptor.api';

describe('Provider: Interceptor - API', () => {
  let provider, sandbox, $injector, $rootScope, $q, $window;

  beforeEach(angular.mock.module('ng', ($provide) => {

  }));

  beforeEach(inject((_$injector_) => {
    $injector = _$injector_;
    $rootScope = $injector.get('$rootScope');
    $q = $injector.get('$q');
    $window = $injector.get('$window');
    provider = new PeachInterceptorAPI.provider();

    sandbox = sinon.sandbox.create();

    $window = { parent: { location: sinon.sandbox.spy() } };
  }));

  afterEach(() => {
    sandbox.restore();
  });

  it('should construct a Peach API Interceptor Provider', () => {
    expect(provider).to.not.be.an('undefined');
  });

  it('should throw an error if trying to call a class as a function', () => {
    expect(() => { PeachInterceptorAPI.provider(); }).to.throw('Cannot call a class as a function');
  });

  it('should construct a peach API intercetpro factory', () => {
    let factory = provider.$get($injector, $q, $window);
    expect(factory).to.be.an('object');
  });

  describe('Functions', () => {
    let factory, MockPeach;

    beforeEach(inject((_$injector_) => {
      factory = provider.$get($injector, $q, $window);

      MockPeach = {
        session: {
          getApiUrl: () => {
            return 'https://my.api.com';
          },
          logout: () => {
            return true;
          }
        }
      };
    }));

    describe('Request', () => {
      let MockConfig;

      beforeEach(() => {
        MockConfig = {
          url: 'https://my.api.com'
        };

        sandbox.stub($injector, 'get').withArgs('$peach').returns(MockPeach);
      });

      it('should simply return the request config if not an API call', () => {
        MockConfig = {
          url: 'https://not.my.api.com'
        };
        expect(factory.request(MockConfig)).to.deep.equal(MockConfig);
      });
    });

    describe('RequestError', () => {
      let MockRejection;

      beforeEach(() => {
        MockRejection = {
          message: 'This is an error message'
        };
      });

      it('should pass the error along, we\'re not doing anything here yet', () => {
        expect(factory.requestError(MockRejection).$$state.value.message).to.be.equal(MockRejection.message);
      });
    });

    describe('Response', () => {
      let MockData;

      beforeEach(() => {
        MockData = {
          value: true
        };
      });

      it('should pass the data along, we\'re not doing anything here yet', () => {
        expect(factory.response(MockData)).to.deep.equal(MockData);
      });
    });

    describe('ResponseError', () => {
      let MockRejection;

      beforeEach(() => {
        MockRejection = {
          message: 'This is an error message',
          config: {
            url: 'https://my.api.com'
          },
          status: 200
        };

        sandbox.stub($injector, 'get').withArgs('$peach').returns(MockPeach);
      });

      it('should pass the error along if not a peach API call', () => {
        MockRejection.config.url = 'https://not.my.api.com';
        expect(factory.responseError(MockRejection).$$state.value.message).to.be.equal(MockRejection.message);
      });

      it('should pass the error along if making logout peach API call', () => {
        MockRejection.config.url = 'https://my.api.com/oauth/session/logout';
        expect(factory.responseError(MockRejection).$$state.value.message).to.be.equal(MockRejection.message);
      });

      it('should pass the error along if making password reset API call', () => {
        MockRejection.config.url = 'https://my.api.com/users/password/reset';
        expect(factory.responseError(MockRejection).$$state.value.message).to.be.equal(MockRejection.message);
      });

      it('should not return to login if the error status is 401 and the user is already on the login page', () => {
        MockRejection.status = 401;
        $window.parent.location.pathname = '/';
        let logoutStub = sandbox.stub(MockPeach.session, 'logout').returns(true);
        factory.responseError(MockRejection);
        sinon.assert.notCalled(logoutStub);
      });

      it('should log the user out and return to login if the error status is 401', () => {
        MockRejection.status = 401;
        let logoutStub = sandbox.stub(MockPeach.session, 'logout').returns(true);
        factory.responseError(MockRejection);
        sinon.assert.calledOnce(logoutStub);
      });
    });
  });
});
